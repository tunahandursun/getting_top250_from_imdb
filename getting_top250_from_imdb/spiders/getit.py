# -*- coding: utf-8 -*-
import scrapy
import psycopg2


class GetitSpider(scrapy.Spider):
    name = 'getit'
    allowed_domains = ['http://www.imdb.com/chart/top']
    start_urls = ['http://www.imdb.com/chart/top/']
    
    def parse(self, response):
        conn = psycopg2.connect(
            database="test", 
            user="user", 
            password="password", 
            host="localhost",
            port=5432,
            )
        cur = conn.cursor()
        cur.execute('''CREATE TABLE IF NOT EXISTS tb_imdb(id SERIAL PRIMARY KEY NOT NULL, title text, rate real); ''')
        conn.commit()
        cur = conn.cursor()
        
        film_titles = response.css('tbody.lister-list tr td.titleColumn a::text').extract()
        film_rates = response.css('tbody.lister-list tr td.ratingColumn strong::text').extract()
        for i in range(1,250):
            cur.execute(
                '''INSERT INTO tb_imdb(title,rate) values(%s,%s);''',
                (
                    film_titles[i], 
                    float(film_rates[i]),
                )
            )
            conn.commit()
